from odoo import models, fields, api, _
from odoo.exceptions import UserError

class InheritAcsLabTest(models.Model):
    _inherit = 'acs.lab.test'

    inspection_package_id = fields.Many2one('mcu.inspection.package', string='Inspection Package', index=True, ondelete='cascade')
    standard_price = fields.Float(related='product_id.standard_price', string="Cost Price", readonly=True)