from odoo import models, fields, api, _
from odoo.exceptions import UserError

class InheritPatient(models.Model):
    _inherit = 'hms.patient'

    nip = fields.Char(string='NIP')

    