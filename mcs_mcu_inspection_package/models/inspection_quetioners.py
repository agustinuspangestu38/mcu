from odoo import models, fields, api

class InspectionQuetioner(models.Model):
    _name = 'inspection.quetioner'
    _description = "Inspection Quetioner"
    _order = 'create_date desc'

    name = fields.Char(string='Name', required=True)
    description = fields.Text(string='Description')
    active = fields.Boolean(string='Active')
    