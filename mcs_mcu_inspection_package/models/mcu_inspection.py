import datetime
from odoo import models, fields, api, _ 

LIST_STATE_MCU = [
    ('draft', 'Draft'),
    ('in_progress', 'In Progress'),
    ('done', 'Done')]

class MCUInspection(models.Model):
    _name = 'mcu.inspection'

    name = fields.Char(string='Name', required=True)
    posting_datetime = fields.Datetime(string='Posting Date', default=datetime.datetime.now())
    patient_id = fields.Many2one('hms.patient', string='Patient', required=True)
    physician_id = fields.Many2one('hms.physician', string='Physician', required=True)
    state = fields.Selection(LIST_STATE_MCU, string='State', default="draft")
    inspection_location_ids = fields.One2many('inspection.location', 'mcu.inspection', string='Lines')


class MCUInspectionPlace(models.Model):
    _name = 'mcu.inspection.placce'

    LIST_STATE = [
        ('draft', 'Draft'),
        ('in_progress', 'In Progress'),
        ('done', 'Done')]

    mcu_inspection_id = fields.Many2one('mcu.inspection', string='MCU Inspection', index=True, ondelete='cascade')
    inspection_location_id = fields.Many2one('inspection.location', string='Location Inspection')
    is_quetioner = fields.Boolean(string='Quetioner ?')
    state = fields.Selection(LIST_STATE, string='State')
    
    jenis_kelamin = fields.Selection([("pria","Pria"),("wanita","Wanita")], string='Jenis Kelamin')

    # Quetioner => General Examination
    riwayat_hepatitis = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwauat Hepatitis')
    desc_hepatitis = fields.Text(string='Description Hepatitis')
    riwayat_pengobatan_tbc = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Pengobatan TBC')
    desc_pengobatan_tbc = fields.Text(string='Description Pengobatan TBC')
    hipertensi_atau_darah_tinggi = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Hipertensi / Darah Tinggi')
    desc_hipertensi_atau_darah_tinggi = fields.Text(string='Desc Hipertensi / Darah Tinggi')
    riwayat_diabetes = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Diabetes')
    desc_diabetes = fields.Text(string='Riwayat Diabetes')
    batuk_menahun = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Batuk Menahun')
    riwayat_operasi = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Operasi')
    desc_riwayat_operasi = fields.Text(string='Riwayat Operasi')
    riwayat_rawat_inap = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Rawat Inap')
    desc_riwayat_rawat_inap= fields.Text(string='Riwayat Rawat Inap')
    riwayat_alergi = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Alergi')
    desc_alergi= fields.Text(string='Riwayat Alergi')
    lain_lain = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Lain lain')
    desc_lain_lain= fields.Text(string='Desc Lain lain')
    riwayat_penyakit_jantung = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Penyakit Jantung')
    desc_penyakit_jantung = fields.Text(string='Desc Penyakit Jantung')
    riwayat_hipertensi = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Hipertensi')
    desc_hipertensi = fields.Text(string='Desc Hipertensi')
    riwayat_stroke = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Stroke')
    desc_stroke = fields.Text(string='Desc Stroke')
    riwayat_penyakit_paru = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Penyakit Paru / Asma / TBC')
    desc_penyakit_paru = fields.Text(string='Desc Penyakit Paru / Asma / TBC')
    riwayat_penyakit_kanker = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Penyakit Kanker / Tumor')
    desc_penyakit_kanker = fields.Text(string='Desc Penyakit Kanker / Tumor')
    riwayat_penyakit_gangguan_jiwa = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Penyakit Gangguan Jiwa')
    desc_penyakit_gangguan_jiwa = fields.Text(string='Desc Penyakit Gangguan Jiwa')
    riwayat_penyakit_ginjal = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Penyakit Ginjal')
    desc_penyakit_ginjal = fields.Text(string='Desc Penyakit Ginjal')
    riwayat_penyakit_saluran_pencernaan = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Saluran Pencernaan')
    desc_penyakit_saluran_pencernaan = fields.Text(string='Desc Saluran Pencernaan')
    riwayat_penyakit_lainnya = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Riwayat Penyakit Lainnya')
    desc_penyakit_lainnya = fields.Text(string='Desc Penyakit Lainnya')
    minum_alkohol = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Minum Alkohol')
    merokok = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='Merokok')
    olahraga = fields.Selection([("ada","Ada"),("tidak","Tidak")], string='olahraga')
    jenis_olahraga = fields.Text(string='Jenis Olahraga')
    olahraga_perminggu = fields.Integer(string='Olahraga per-minggu')

    ######################
    # CLINICAL ASSESMENT #
    ######################

    LIST_KATEGORI_TD = [("normal","Normal"),
                        ("prehipertensi","Prehipertensi"), 
                        ("hipertensi_stadium_1", "Hipertensi Stadium 1"),
                        ("hipertensi_stadium_2", "Hipertensi Stadium 2")]
    LIST_KATEGORI_REFRAKSI = [
        ('myopia', 'Myopia'), 
        ('presbiopia', 'Presbiopia'), 
        ('hypermetropi', 'Hipermetropi'), 
        ('astigmatism', 'Astigmatism')]

    tensi_sistole = fields.Float(string='Tanda Vital - Tensi (Sistole mmHg)')
    tensi_diastole = fields.Float(string='Tanda Vital - Tensi (Diastole mmHg)')
    kategori_td = fields.Char(string='Kategori Tekanan Darah')
    nadi = fields.Float(string='Tanda Vital - Nadi X/menit')
    rr = fields.Float(string='Tanda Vital - RR')
    suhu = fields.Float(string='Tanda Vital - Suhu')
    spo2 = fields.Float(string='Tanda Vital - SPO2(%)')
    berat_badan = fields.Float(string='Tanda Vital - Berat Badan (kg)')
    tinggi_badan = fields.Float(string='Tanda Vital - Tinggi Badan (cm)')
    lingkar_perut = fields.Float(string='Tanda Vital - Lingkar Perut (cm)')
    visus_ka = fields.Integer(string='Tanda vital - Visus (Ka)')
    visus_ki = fields.Integer(string='Tanda Vital - Visus (Ki)')
    kategori_refraksi = fields.Selection(LIST_KATEGORI_REFRAKSI, string='Kategori Refraksi')
    bmi = fields.Float(string='Tanda Vital - BMI')
    kategori_bmi = fields.Char(string='Kategori BMI')

    #####################
    # Pemeriksaan Fisik #
    #####################

    kulit = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Kulit')
    desc_kulit = fields.Text(string='Desc Kulit')
    kesadaran_umum = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Kesadaran Umum')
    desc_kesadaran_umum = fields.Text(string='Desc Kesadaran Umum')
    kesadaran_mental = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Kesadaran Mental')
    desc_kesadaran_mental= fields.Text(string='Desc Kesadaran Mental')
    mata = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Mata')
    desc_mata = fields.Text(string='Desc Mata')
    buta_warna = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Buta Warna')
    desc_buta_warna = fields.Text(string='Desc Buta Warna')
    kelainan_mata = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Kelainan Mata')
    desc_kelainan_mata = fields.Text(string='Desc Kelainan Warna')
    tht = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='THT')
    desc_tht = fields.Text(string='Desc THT')
    telinga = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Telinga')
    desc_telinga = fields.Text(string='Desc Telinga')
    hidung = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Hidung')
    desc_hidung = fields.Text(string='Desc Hidung')
    tenggorokan = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Tenggorokan')
    desc_tenggorokan = fields.Text(string='Desc Tenggorokan')
    sinus = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Sinus')
    desc_sinus = fields.Text(string='Desc Sinus')
    tonsil = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Tonsil')
    desc_tonsil = fields.Text(string='Desc Tonsil')
    desc_gigi_dan_mulut = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Gigi dan Mulut')
    desc_desc_gigi_dan_mulut = fields.Text(string='Desc Gigi dan Mulut')
    gusi = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Gusi')
    desc_gusi = fields.Text(string='Desc Gusi')
    kepala_dan_leher = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Kepala dan Leher')
    desc_kepala_dan_leher = fields.Text(string='Desc Kepala dan Leher')
    kelenjar_limfe = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Kelenjar Limfe')
    desc_kelenjar_limfe = fields.Text(string='Desc Kelenjar Limfe')
    kelenjar_tiroid = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Kelenjar Tiroid')
    desc_kelenjar_tiroid = fields.Text(string='Desc Kelenjar Tiroid')
    dada = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Dada')
    desc_dada = fields.Text(string='Desc Dada')
    jantung = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Jantung')
    desc_jantung = fields.Text(string='Desc Jantung')
    paru = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Paru')
    desc_paru = fields.Text(string='Desc Paru')
    tulang_belakang = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Tulang Belakang')
    desc_tulang_belakang = fields.Text(string='Desc Tulang Belakang')
    abdomen = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Abdomen')
    desc_abdomen = fields.Text(string='Desc Abdomen')
    perabaan = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Perabaan')
    desc_perabaan = fields.Text(string='Desc Perabaan')
    hati = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Hati')
    desc_hati = fields.Text(string='Desc Hati')
    ginjal = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Ginjal')
    desc_ginjal = fields.Text(string='Desc Ginjal')
    extrimitas = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Extrimitas')
    desc_extrimitas = fields.Text(string='Desc Extrimitas')
    neurologis = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Neurologis')
    desc_neurologis = fields.Text(string='Desc Neurologis')

    ################
    # LABORATORIUM #
    ################

    hemoglobin = fields.Integer(string='Hemoglobin (g/dl)')
    kategori_anemia = fields.Char(string='Product name', compute="_compute_kategori_anemia", store=True)
    hematokrit = fields.Float(string='Hematokrit')
    eritrosit = fields.Float(string='Eritrosit')
    mcv = fields.Float(string='MCV')
    mch = fields.Float(string='MCH')
    mcmc = fields.Float(string='MCMC')
    rdw = fields.Float(string='RDW')
    trombosit = fields.Float(string='Trombosit')
    kategori_trombositosis = fields.Char(string='Kategori Trombosit', compute="_compute_kategori_trombositosis", store=True)
    leukosit = fields.Float(string='Leukosit (H)')
    kategori_lekositosis = fields.Char(string='Kategori Lekosistosis', compute="_compute_kategori_lekositosis", store=True)
    basofil = fields.Float(string='Basofil')
    eosinofil = fields.Float(string='Eosinofil')
    neutrofil = fields.Float(string='Neutrofil')
    limfosit = fields.Float(string='Limfosit')
    monosit = fields.Float(string='Monosit')
    led = fields.Float(string='LED')
    kategori_led = fields.Char(string='Kategori LED', compute="_compute_categori_led", store=True)
    sgot = fields.Float(string='SGOT (µ/L)')
    kategori_sgot = fields.Char(string='Kategori SGOT', compute="_compute_categori_sgot", store=True)
    sgpt = fields.Float(string='SGPT (µ/L)')
    kategori_sgpt = fields.Char(string='Kategori SGPT', compute="_compute_categori_sgpt", store=True)
    ureum = fields.Float(string='Ureum')
    kategori_uremia = fields.Char(string='Kategori Uremia', compute="_compute_category_uremia", store=True)
    kreatinin = fields.Float(string='Kreatinin (mg/dL)')
    kategori_kreatinin = fields.Char(string='Kategori_kreatinin', compute="_compute_kategori_kreatinin", store=True)
    egfr = fields.Float(string='EGFR')
    asam_urat = fields.Float(string='Asam Urat')
    kategori_hiperuresemia = fields.Char(string='Kategori Hiperuresemia', compute="_compute_kategori_hiperuresemia", store=True)
    glukosa_puasa = fields.Float(string='Glukosa Puasa')
    kategori_hiperglikemia = fields.Char(string='Kategori Hiperglikemia', compute="_compute_hiperglikemia", store=True)
    glukosa_sewaktu = fields.Float(string='Glukosa Sewaktu')
    glukosa_2jpp = fields.Float(string='Glukosa 2JPP')
    kolesterol_total = fields.Float(string='Kolesterol Total')
    kategori_hiperkolesterol = fields.Char(string='Kategori Hiperkolesterol', compute="_compute_hiperkolesterol", store=True)
    trigliserida = fields.Float(string='Trigliserida (mg/dl)')
    kategori_hiperlipidemia = fields.Char(string='Kategori Hiperlipidemia', compute="_compute_hiperlipidemia", store=True)
    hdl = fields.Float(string='HDL')
    ldl = fields.Float(string='LDL')
    warna = fields.Char(string='Warna')
    kejernihan = fields.Selection([("jenih","Jernih"),("agak_keruh","Agak Keruh"), ("keruh", "Keruh")], string='Kejernihan')
    berat_jenis = fields.Float(string='Berat Jenis')
    ph = fields.Float(string='PH')
    leukosit = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Leukosit')
    eritrosit = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Eritrosit')
    nitrit = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Nitrit')
    albumin = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Albumin')
    glukosa = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Glukosa')
    keton = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Keton')
    urobilinogen = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Urobilinogen')
    bilirubin = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Bilirubin')
    bilirubin = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Bilirubin')
    desc_eritrosit = fields.Char(string='Eritrosit')
    desc_leukosit = fields.Char(string='Leukosit')
    desc_leukosit = fields.Char(string='Leukosit')
    epitel = fields.Char(string='Epitel')
    silinder = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Silinder')
    kristal = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Kristal')
    bakteri = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Bakteri')
    pemeriksaan_lainnya = fields.Boolean(string='Pemeriksaan Lainnya')
    permiksaan_lain_lain = fields.Text(string='Periksaan Lain - lain')
    apetamin = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Apetamin')
    connabis = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Connabis')
    opiat = fields.Selection([("negatif","Negatif"),("positif","Positif")], string='Opiat')
    pemeriksaan_attch = fields.Binary(string='Pemeriksaan Attch')

    @api.depends('trigliserida')
    def _compute_hiperlipidemia(self):
        for rec in self:
            rec.kategori_hiperlipidemia = "Hiperlipidemia" if rec.trigliserida < 150 else "Normal"

    @api.depends('kolesterol_total')
    def _compute_hiperkolesterol(self):
        for rec in self:
            rec.kategori_hiperkSpacing = "Hiperkolesterol" if rec.kolesterol_total < 300 else "Normal"

    @api.depends('glukosa_puasa')
    def _compute_hiperglikemia(self):
        for rec in self:
            rec.kategori_hiperglikemia = "Hiperglikemia" if rec.glukosa_puasa < 300 else "Normal" 

    @api.depends('asam_urat')
    def _compute_kategori_hiperuresemia(self):
        for rec in self:
            rec.kategori_hiperuresemia = "Normal"
            if (rec.jenis_kelamin == "wanita" and rec.asam_urat > 6.0) or \
                (rec.jenis_kelamin == "pria" and rec.asam_urat > 7.0):
                rec.kategori_hiperuresemia = "Hiperuresemia"

    @api.depends('kreatinin')
    def _compute_kategori_kreatinin(self):
        for rec in self:
            rec.kategori_kreatinin = "Normal" 
            if (rec.kreatinin > 1.2 and rec.jenis_kelamin == "wanita") or \
                (rec.kreatinin > 1.4 and rec.jenis_kelamin == "pria"):
                rec.kategori_kreatinin = "Abnormal"

    @api.depends('ureum')
    def _compute_category_uremia(self):
        for rec in self:
            rec.kategori_uremia = "Normal" if rec.ureum < 40 else "Uremia"

    @api.depends('sgpt')
    def _compute_categori_sgpt(self):
        for rec in self:
            rec.kategori_sgpt = "Normal" if rec.sgpt < 40 else "Abnormal"

    @api.depends('sgot')
    def _compute_categori_sgot(self):
        for rec in self:
            rec.kategori_sgot = "Normal" if rec.sgot < 20 else "Abnormal"

    @api.depends('led')
    def _compute_categori_led(self):
        for rec in self:
            rec.kategori_led = "Normal" 
            if (rec.jenis_kelamin == "pria" and rec.led > 20) or \
                (rec.jenis_kelamin == "wanita" and rec.led > 15): 
                rec.kategori_led = "Peningkatan LED"

    @api.depends('leukosit')
    def _compute_kategori_lekositosis(self):
        for rec in self:
            rec.kategori_lekositosis = "Normal" if rec.leukosit < 10 else "Lekositosis"

    @api.depends('trombosit')
    def _compute_kategori_trombositosis(self):
        for rec in self:
            rec.kategori_trombositosis = 'Normal' if rec.trombosit < 400 else 'Trombositosis'

    @api.depends('hemoglobin')
    def _compute_kategori_anemia(self):
        for rec in self:
            rec.kategori_anemia = 'Normal' if rec.hemoglobin < 11 else 'Anemia'
    
    ########
    # EKG #
    #######
            
    kesimpulan = fields.Text(string='Kesimpulan')
    frekuensi = fields.Integer(string='Frekuensi')
    aksis = fields.Selection([("normo_aksis","Normo Aksis"),("lad","LAD"), ('rad', 'RAD'), ('erad', 'ERAD')], string='Aksis')
    gelombang_p = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Gelombang P')
    desc_gelombang_p = fields.Char(string='Desc Gelombang P')
    interval_rr = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Interval RR')
    desc_interval_rr = fields.Char(string='Desc Interval RR')
    kompleks_qrs = fields.Char(string='Kompleks QRS')
    gelombang_q_patogis = fields.Selection([("ada","Ada"),("tidak_ada","tidak_ada")], string='Gelombang Q Patogis')
    segmen_st = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Segmen ST')
    desc_segmen_st = fields.Char(string='Desc Segmen ST')
    gelombang_t = fields.Selection([("normal","Normal"),("abnormal","Abnormal")], string='Gelombang T')
    desc_segmen_t = fields.Char(string='Desc Segmen T')
    interval_qt = fields.Selection([("memanjang","Memanjang"),("tidak_memanjang","Tidak Memanjang")], string='Interval QT')
    desc_interval_qt = fields.Char(string='Desc Interval QT')
    hipertrofi_ventrikel = fields.Char(string='Hipertrofi Ventrikel')
    ekg_attch = fields.Binary(string='EKG attachment')

    #############
    # RADIOLOGI #
    #############

    kesimpulan = fields.Text(string='Kesimpulan')
    nama_pemeriksa = fields.Char(string='Nama Pemeriksa')
    cor = fields.Selection([("bentuk_dan_besar_normal","Bentuk dan Besar Normal"),("abnormal","Abnormal")], string='COR')
    rad_attch = fields.Binary(string='Radiology attachment')

    ############
    # ANALISIS #
    ############

    kesimpulan = fields.Text(string='Kesimpulan')
    saran = fields.Text(string='Saran')
    fix_with_note = fields.Selection([("fit_to_work","Fit to Work"),("fit_with_note","Fit With Note"), ('temporary_unfit', 'Temporary Unfit')], string='Fix With Notes')
    nama_dokter = fields.Char(string='Nama Dokter')
    tanggal_pemeriksaan = fields.Date(string='Tanggal_ Pemeriksaan')