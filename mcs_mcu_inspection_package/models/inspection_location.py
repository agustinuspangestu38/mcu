from odoo import models, fields, api

class InspectionLocation(models.Model):
    _name = 'inspection.location'
    _description = "Inspection Location"
    _order = "create_date desc"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(string='Name', required=True)
    description = fields.Text(string='Description')
    active = fields.Boolean(string='Active?', default=True)