from odoo import models, fields, api, _

class MCUInspectionPackage(models.Model):
    _name = 'mcu.inspection.package'
    _description = "MCU Inspection Package"
    _rec_name = 'code'
    _order = 'create_date desc'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    code = fields.Char(string='Code', tracking=True)
    name = fields.Char(string='Name', required=True, tracking=True)
    description = fields.Text(string='Description', tracking=True)
    inspection_location_unique_ids = fields.Many2many(
        comodel_name='inspection.location', 
        string='Inspection Place Unique',
        compute="_compute_inspection_location_unique",
        store=True)
    inspection_location_ids = fields.One2many('mcu.inspection.location', 'inspection_package_id', string='Inspection Places', tracking=True)
    lab_inspection = fields.Boolean(string='Lab Inspection?', default=False, tracking=True)
    panthology_ids = fields.Many2many('acs.lab.test', 'inspection_package_id', string='Panthologys', 
                                    domain=[('test_type', '=', 'panthology')], tracking=True)
    rad_inspection = fields.Boolean(string='Radiologi Inspection?', default=False, tracking=True)
    radiology_ids = fields.Many2many('acs.lab.test', 'inspection_package_id', string='Radiology', 
                                    domain=[('test_type', '=', 'radiology')], tracking=True)
    active = fields.Boolean(string='Active', default=True, tracking=True)    

    @api.depends('inspection_location_ids', 'inspection_location_ids.sequence', 'inspection_location_ids.inspection_location_id')
    def _compute_inspection_location_unique(self):
        for rec in self:
            rec.inspection_location_unique_ids = [(4, line.inspection_location_id.id) \
                                               for line in rec.inspection_location_ids if line.inspection_location_id]
            
    @api.model
    def create(self, vals):
        vals['code'] = self.env['ir.sequence'].next_by_code('mcu_inspection_package') or _('New')
        return super().create(vals)  


class MCUInspectionPlaces(models.Model):
    _name = 'mcu.inspection.location'
    _description = 'MCU Inspection Place'

    inspection_package_id = fields.Many2one('mcu.inspection.package', string='Inspection Package', index=True, ondelete='cascade')
    inspection_location_id = fields.Many2one('inspection.location', required=True, string='Inspection Place', index=True, ondelete='restrict')
    sequence = fields.Integer(string='Sequence')
    lab_test_id = fields.Many2one('acs.lab.test', string='Lab Test')

    lab_inspection = fields.Boolean(string='Lab Inspection?', default=False, tracking=True, help="Laboratory Inspection")
    # panthology_ids = fields.Many2many('acs.lab.test', 'inspection_package_id', string='Panthologys', 
    #                                 domain=[('test_type', '=', 'panthology')], tracking=True)
    rad_inspection = fields.Boolean(string='Rad Inspection?', default=False, tracking=True)
    # radiology_ids = fields.Many2many('acs.lab.test', 'inspection_package_id', string='Radiology', help="Radiology Inspection",
    #                                 domain=[('test_type', '=', 'radiology')], tracking=True)