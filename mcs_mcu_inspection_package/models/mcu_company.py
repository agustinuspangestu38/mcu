from odoo import models, fields, api

class MCUCompany(models.Model):
    _name = 'mcu.company'
    _description = "Company MCU"
    _order = 'create_date desc'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    code = fields.Char(string='Code', tracking=True)
    name = fields.Char(string='Name', required=True, tracking=True)
    description = fields.Text(string='Description', tracking=True)
    logo = fields.Binary(string='Logo', tracking=True)
    active = fields.Boolean(string='Active?', default=True, required=True, tracking=True)

    @api.model
    def create(self, vals):
        vals['code'] = self.env['ir.sequence'].next_by_code('mcu_company') or _('New')
        return super().create(vals)  