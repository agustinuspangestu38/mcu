# -*- coding: utf-8 -*-
{
    'name': "MCS | MCU Inspection Package",

    'summary': """
        Matrica plays a part in coloring the future of IT business with its products and services, guaranteed its quality, advances, sophistication and reliability""",

    'description': """
        founded in 2005, information technology consulting company specializing in application development and integration, and IT strategic planning
    """,

    'author': "Agustinus Pangestu Wijaya",
    'website': "http://www.matrica.co.id",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'MCSModule',
    'version': '0.1',

    # any module necessary for this one to work correctly
    # 'depends': ['base', 'mail', 'acs_laboratory', 'acs_hms_laboratory', 'acs_hms_vital_examination'],
    'depends': ['base', 'mail', 'acs_hms_base', 'mcs_acs_lab_test'],
    'license': 'OEEL-1',

    # always loaded
    'data': [
        'data/ir_sequence_mcu_inspection_package.xml',
        'data/ir_sequence_mcu_company.xml',
        'security/ir.model.access.csv',
        # 'views/inherit_hms_patient.xml',
        'views/inspection_location.xml',
        'views/mcu_company.xml',
        # 'views/mcu_inspection_package.xml',
        'wizard/mcu_import.xml',
        'views/menuitems.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
