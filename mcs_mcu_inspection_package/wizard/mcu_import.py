import datetime
from odoo import models, fields,_
import openpyxl
import base64
from io import BytesIO
from odoo.exceptions import UserError

class MCUImportWizard(models.TransientModel):
   _name = "import.data.wizard"
   file = fields.Binary(string="File", required=True)

   def map_gender(self, val):
      gender = "other"
      if "pria" in val.lower().replace(' ', ''):
         gender = "male"
      elif "wanita" in val.lower().replace(' ', ''):
         gender = "female"
      return gender
   
   def map_date(self, val, separator):
      if type(val) == datetime.datetime:
         return val.strftime("%Y-%m-%d-")
      else:
         split_date = val.split(separator)
         return split_date[2]+'-'+split_date[1]+'-'+split_date[0]
   
   def get_package_name(self, vals, separator):
      split_vals = vals.split(separator)
      if len(split_vals) > 1:
         return split_vals[0].strip()
      else:
         return ""

   def import_data(self):
      try:
         wb = openpyxl.load_workbook(filename=BytesIO(base64.b64decode(self.file)), read_only=True)
         ws = wb.active
         for record in ws.iter_rows(min_row=2, max_row=None, min_col=None, max_col=None, values_only=True):
            
            exist_patient = self.env['hms.patient'].search([('name', '=', record[3])])
            if not exist_patient:
               self.env['hms.patient'].create({
                  'code': record[1],
                  'nip': record[2],
                  'name': record[3],
                  # 'kode': record[4],
                  'gov_code': record[5],
                  'mobile': record[6],
                  'birthday': self.map_date(record[7], '/'),
                  'gender': self.map_gender(record[8])
               })

            exist_company = self.env['mcu.company'].search([('name', '=', record[9])])
            if not exist_company:
               self.env['mcu.company'].create({
                  'perusahaan': self.env['mcu.company'].search('name', '=', record[9])
               })

            package_name = self.get_package_name(record[10], ' :')
            inspection_package_id = self.env['mcu.inspection.package'].search([('name', '=', record[10])])
            if not inspection_package_id:
               self.env['mcu.inspection.package'].create({'name': package_name})               
               
      except Exception as e:
         if self.env.user.has_group('base.group_no_one'):
            raise UserError(_('Please insert a valid file. Error: '+str(e)))
         else:
            raise UserError(_('Please insert a valid file.'))